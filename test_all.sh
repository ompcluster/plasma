#!/bin/bash

set -e
set -x

# Test plasma
kernels=(
    sgemm
    strsm
    ssyrk
    spotrf
)
dim=(
    1024
    2048
)
nb=(
    256
    512
)

for k in "${kernels[@]}"; do
    for d in "${dim[@]}"; do
        for n in "${nb[@]}"; do
            ./build/plasmatest $k --dim=$d --nrhs=$d --nb=$n --nb_inner=64
            mpirun -np 3 ./build/plasmatest $k --dim=$d --nrhs=$d --nb=$n --nb_inner=64
            mpirun -np 5 ./build/plasmatest $k --dim=$d --nrhs=$d --nb=$n --nb_inner=64
        done
    done
done

