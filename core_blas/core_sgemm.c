/**
 *
 * @file
 *
 *  PLASMA is a software package provided by:
 *  University of Tennessee, US,
 *  University of Manchester, UK.
 *
 * @generated from /home/luszczek/workspace/plasma/bitbucket/plasma/core_blas/core_zgemm.c, normal z -> s, Fri Sep 28 17:38:18 2018
 *
 **/

// #include "plasma_descriptor.h"
// #include "plasma_internal.h"
// #include <plasma_core_blas.h>
// #include "core_lapack.h"

#pragma omp declare target
#include "plasma_s.h"
#include "plasma_context.h"
#include <plasma_core_blas.h>
#include "plasma_types.h"
#include "core_lapack.h"
#pragma omp end declare target

/***************************************************************************//**
 *
 * @ingroup core_gemm
 *
 *  Performs one of the matrix-matrix operations
 *
 *    \f[ C = \alpha [op( A )\times op( B )] + \beta C, \f]
 *
 *  where op( X ) is one of:
 *    \f[ op( X ) = X,   \f]
 *    \f[ op( X ) = X^T, \f]
 *    \f[ op( X ) = X^T, \f]
 *
 *  alpha and beta are scalars, and A, B and C  are matrices, with op( A )
 *  an m-by-k matrix, op( B ) a k-by-n matrix and C an m-by-n matrix.
 *
 *******************************************************************************
 *
 * @param[in] transa
 *          - PlasmaNoTrans:   A is not transposed,
 *          - PlasmaTrans:     A is transposed,
 *          - PlasmaConjTrans: A is conjugate transposed.
 *
 * @param[in] transb
 *          - PlasmaNoTrans:   B is not transposed,
 *          - PlasmaTrans:     B is transposed,
 *          - PlasmaConjTrans: B is conjugate transposed.
 *
 * @param[in] m
 *          The number of rows of the matrix op( A ) and of the matrix C.
 *          m >= 0.
 *
 * @param[in] n
 *          The number of columns of the matrix op( B ) and of the matrix C.
 *          n >= 0.
 *
 * @param[in] k
 *          The number of columns of the matrix op( A ) and the number of rows
 *          of the matrix op( B ). k >= 0.
 *
 * @param[in] alpha
 *          The scalar alpha.
 *
 * @param[in] A
 *          An lda-by-ka matrix, where ka is k when transa = PlasmaNoTrans,
 *          and is m otherwise.
 *
 * @param[in] lda
 *          The leading dimension of the array A.
 *          When transa = PlasmaNoTrans, lda >= max(1,m),
 *          otherwise, lda >= max(1,k).
 *
 * @param[in] B
 *          An ldb-by-kb matrix, where kb is n when transb = PlasmaNoTrans,
 *          and is k otherwise.
 *
 * @param[in] ldb
 *          The leading dimension of the array B.
 *          When transb = PlasmaNoTrans, ldb >= max(1,k),
 *          otherwise, ldb >= max(1,n).
 *
 * @param[in] beta
 *          The scalar beta.
 *
 * @param[in,out] C
 *          An ldc-by-n matrix. On exit, the array is overwritten by the m-by-n
 *          matrix ( alpha*op( A )*op( B ) + beta*C ).
 *
 * @param[in] ldc
 *          The leading dimension of the array C. ldc >= max(1,m).
 *
 ******************************************************************************/
#pragma omp declare target
__attribute__((weak))
void plasma_core_sgemm(plasma_enum_t transa, plasma_enum_t transb,
                int m, int n, int k,
                float alpha, const float *A, int lda,
                                          const float *B, int ldb,
                float beta,        float *C, int ldc)
{
    cblas_sgemm(CblasColMajor,
                (CBLAS_TRANSPOSE)transa, (CBLAS_TRANSPOSE)transb,
                m, n, k,
                (alpha), A, lda,
                                    B, ldb,
                (beta),  C, ldc);
}
#pragma omp end declare target

/******************************************************************************/
void plasma_core_ompc_sgemm(
    plasma_enum_t transa, plasma_enum_t transb,
    int m, int n, int k,
    float alpha, float *pA, int lda,
                              float *pB, int ldb,
    float beta,        float *pC, int ldc,
    plasma_sequence_t *sequence, plasma_request_t *request,
    int inner_nb)
{
    int ak;
    if (transa == PlasmaNoTrans)
        ak = k;
    else
        ak = m;

    int bk;
    if (transb == PlasmaNoTrans)
        bk = n;
    else
        bk = k;

    int trana = transa;
    int tranb = transb;
    int sucess = PlasmaSuccess;
    int tuning = PlasmaTuning;
    int disabled = PlasmaDisabled;
    int plasma_nb = PlasmaNb;

    if (sequence->status == sucess){
       #pragma omp target nowait \
                depend(in:*pA) \
                depend(in:*pB) \
                depend(inout:*pC) \
                firstprivate(trana, tranb, m, n, k, alpha, lda, ldb)\
                firstprivate(beta, ldc, inner_nb)
        {
            plasma_init();
            plasma_set(plasma_nb, inner_nb);
            plasma_set(tuning, disabled);

            int ret = plasma_sgemm(
                trana, tranb,
                m, n, k,
                alpha, pA, lda,
                pB, ldb,
                beta, pC, ldc);

            plasma_finalize();
        }
    }
}

#pragma omp declare target
void plasma_core_omp_sgemm(
    plasma_enum_t transa, plasma_enum_t transb,
    int m, int n, int k,
    float alpha, const float *A, int lda,
                              const float *B, int ldb,
    float beta,        float *C, int ldc,
    plasma_sequence_t *sequence, plasma_request_t *request)
{
    int ak;
    if (transa == PlasmaNoTrans)
        ak = k;
    else
        ak = m;

    int bk;
    if (transb == PlasmaNoTrans)
        bk = n;
    else
        bk = k;

    #pragma omp task depend(in:A[0:lda*ak]) \
                     depend(in:B[0:ldb*bk]) \
                     depend(inout:C[0:ldc*n])
    {
        if (sequence->status == PlasmaSuccess)
            plasma_core_sgemm(transa, transb,
                       m, n, k,
                       alpha, A, lda,
                              B, ldb,
                       beta,  C, ldc);
    }
}
#pragma omp end declare target
