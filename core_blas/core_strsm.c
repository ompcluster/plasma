/**
 *
 * @file
 *
 *  PLASMA is a software package provided by:
 *  University of Tennessee, US,
 *  University of Manchester, UK.
 *
 * @generated from /home/luszczek/workspace/plasma/bitbucket/plasma/core_blas/core_ztrsm.c, normal z -> s, Fri Sep 28 17:38:19 2018
 *
 **/
#pragma omp declare target
#include "plasma_s.h"
#include "plasma_context.h"
#include <plasma_core_blas.h>
#include "plasma_types.h"
#include "core_lapack.h"
#pragma omp end declare target
/***************************************************************************//**
 *
 * @ingroup core_trsm
 *
 *  Solves one of the matrix equations
 *
 *    \f[ op( A )\times X  = \alpha B, \f] or
 *    \f[ X \times op( A ) = \alpha B, \f]
 *
 *  where op( A ) is one of:
 *    \f[ op( A ) = A,   \f]
 *    \f[ op( A ) = A^T, \f]
 *    \f[ op( A ) = A^T, \f]
 *
 *  alpha is a scalar, X and B are m-by-n matrices, and
 *  A is a unit or non-unit, upper or lower triangular matrix.
 *  The matrix X overwrites B.
 *
 *******************************************************************************
 *
 * @param[in] side
 *          - PlasmaLeft:  op(A)*X = B,
 *          - PlasmaRight: X*op(A) = B.
 *
 * @param[in] uplo
 *          - PlasmaUpper: A is upper triangular,
 *          - PlasmaLower: A is lower triangular.
 *
 * @param[in] transa
 *          - PlasmaNoTrans:   A is not transposed,
 *          - PlasmaTrans:     A is transposed,
 *          - PlasmaConjTrans: A is conjugate transposed.
 *
 * @param[in] diag
 *          - PlasmaNonUnit: A has non-unit diagonal,
 *          - PlasmaUnit:    A has unit diagonal.
 *
 * @param[in] m
 *          The number of rows of the matrix B. m >= 0.
 *
 * @param[in] n
 *          The number of columns of the matrix B. n >= 0.
 *
 * @param[in] alpha
 *          The scalar alpha.
 *
 * @param[in] A
 *          The lda-by-ka triangular matrix,
 *          where ka = m if side = PlasmaLeft,
 *            and ka = n if side = PlasmaRight.
 *          If uplo = PlasmaUpper, the leading k-by-k upper triangular part
 *          of the array A contains the upper triangular matrix, and the
 *          strictly lower triangular part of A is not referenced.
 *          If uplo = PlasmaLower, the leading k-by-k lower triangular part
 *          of the array A contains the lower triangular matrix, and the
 *          strictly upper triangular part of A is not referenced.
 *          If diag = PlasmaUnit, the diagonal elements of A are also not
 *          referenced and are assumed to be 1.
 *
 * @param[in] lda
 *          The leading dimension of the array A. lda >= max(1,k).
 *
 * @param[in,out] B
 *          On entry, the ldb-by-n right hand side matrix B.
 *          On exit, if return value = 0, the ldb-by-n solution matrix X.
 *
 * @param[in] ldb
 *          The leading dimension of the array B. ldb >= max(1,m).
 *
 ******************************************************************************/
#pragma omp declare target
__attribute__((weak))
void plasma_core_strsm(plasma_enum_t side, plasma_enum_t uplo,
                plasma_enum_t transa, plasma_enum_t diag,
                int m, int n,
                float alpha, const float *A, int lda,
                                                float *B, int ldb)
{
    cblas_strsm(CblasColMajor,
                (CBLAS_SIDE)side, (CBLAS_UPLO)uplo,
                (CBLAS_TRANSPOSE)transa, (CBLAS_DIAG)diag,
                m, n,
                (alpha), A, lda,
                                    B, ldb);
}
#pragma omp end declare target

/******************************************************************************/
void plasma_core_ompc_strsm(
    plasma_enum_t side, plasma_enum_t uplo,
    plasma_enum_t transa, plasma_enum_t diag,
    int m, int n,
    float alpha, float *pA, int lda,
    float *pB, int ldb,
    plasma_sequence_t *sequence, plasma_request_t *request,
    int inner_nb)
{
    int ak;
    if (side == PlasmaLeft)
        ak = m;
    else
        ak = n;

    int up = uplo;
    int tran = transa;
    int dia = diag;
    int si = side;
    int sucess = PlasmaSuccess;
    int tunning = PlasmaTuning;
    int disabled = PlasmaDisabled;
    int plasma_nb = PlasmaNb;

    if (sequence->status == sucess) {
        #pragma omp target nowait \
                depend(in:*pA) \
                depend(inout:*pB) \
                firstprivate(si,up,tran,dia,m,n,alpha,lda,ldb)\
                firstprivate(ak,plasma_nb,inner_nb)
        {
            plasma_init();
            plasma_set(plasma_nb, inner_nb);
            plasma_set(tunning, disabled);

            int ret = plasma_strsm(
                si, up,
                tran, dia,
                m, n,
                alpha, pA, lda,
                pB, ldb);

            plasma_finalize(); 
        }
    }
}

#pragma omp declare target
void plasma_core_omp_strsm(
    plasma_enum_t side, plasma_enum_t uplo,
    plasma_enum_t transa, plasma_enum_t diag,
    int m, int n,
    float alpha, const float *A, int lda,
                                    float *B, int ldb,
    plasma_sequence_t *sequence, plasma_request_t *request)
{
    int ak;
    if (side == PlasmaLeft)
        ak = m;
    else
        ak = n;

    int up = uplo;
    int tran = transa;
    int sucess = PlasmaSuccess;

    #pragma omp task depend(in:A[0:lda*ak]) \
                     depend(inout:B[0:ldb*n])
    {
        if (sequence->status == sucess)
            plasma_core_strsm(side, up,
                       tran, diag,
                       m, n,
                       alpha, A, lda,
                              B, ldb);
    }
}
#pragma omp end declare target
